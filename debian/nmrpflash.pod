=head1 NAME

nmrpflash -- firmware flash utility for Netgear devices

=head1 USAGE

nmrpflash [I<OPTION>...]

=head1 OPTIONS

B<-i>, and B<-f> or B<-c> are mandatory

=over

=item B<-a> I<ipaddr>

IP address to assign to target device

=item B<-A> I<ipaddr>

IP address to assign to selected interface

=item B<-B>

Blind mode (don't wait for response packets)

=item B<-c> I<command>

Command to run before (or instead of) TFTP upload

=item B<-f> I<firmware>

Firmware file

=item B<-F> I<filename>

Remote filename to use during TFTP upload

=item B<-i> I<interface>

Network interface directly connected to device

=item B<-m> I<mac>

MAC address of target device (xx:xx:xx:xx:xx:xx)

=item B<-M> I<netmask>

Subnet mask to assign to target device

=item B<-t> I<timeout>

Timeout (in milliseconds) for NMRP packets

=item B<-T> I<timeout>

Time (seconds) to wait after successful TFTP upload

=item B<-p> I<port>

Port to use for TFTP upload

=item B<-R> I<region>

Set device region (NA, WW, GR, PR, RU, BZ, IN, KO, JP)

=item B<-v>

Be verbose

=item B<-V>

Print version and exit

=item B<-L>

List network interfaces

=item B<-h>

Show this screen

=back

=head1 EXAMPLES

(run as root)

 # nmrpflash -i eth0 -f firmware.bin

When using -c, the environment variables IP, PORT, NETMASK
and MAC are set to the device IP address, TFTP port, subnet
mask and MAC address, respectively.

=head1 COPYRIGHT & LICENSE

nmrpflash Copyright (C) 2016 Joseph C. Lehner

nmrpflash is free software, licensed under the GNU GPLv3.
Source code at L<https://github.com/jclehner/nmrpflash>

This documentation was created by Damyan Ivanov L<dmn@debian.org> from the
C<nmrpflash> help text. Licensed under the same terms as the upstream source
code.
